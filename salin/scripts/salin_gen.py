#########################################################################
# Salin - scripts to insert attc casette in genomes                     #
# Authors: Bertrand Neron                                               #
# Copyright (c) 2023  Institut Pasteur (Paris).                         #
# See the COPYRIGHT file for details                                    #
#                                                                       #
# This file is part of MacSyFinder package.                             #
#                                                                       #
# MacSyFinder is free software: you can redistribute it and/or modify   #
# it under the terms of the GNU General Public License as published by  #
# the Free Software Foundation, either version 3 of the License, or     #
# (at your option) any later version.                                   #
#                                                                       #
# salin is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of        #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          #
# GNU General Public License for more details .                         #
#                                                                       #
# You should have received a copy of the GNU General Public License     #
# along with salin (COPYING).                                           #
# If not, see <https://www.gnu.org/licenses/>.                          #
#########################################################################

import os
import sys
import argparse
import logging

from Bio import SeqIO

_log = logging.getLogger('salin')

stdout_handler = logging.StreamHandler(sys.stdout)
stdout_formatter = logging.Formatter("%(levelname)s: %(message)s",
                                     datefmt=None,
                                     style='%'
                                     )
stdout_handler.setFormatter(stdout_formatter)
_log.addHandler(stdout_handler)

from salin.salin import get_cassette, get_positions_of_insertion, CassetteInjector, get_insertion_site, get_junctions


def parse_args(args):
    """

    :param args: the command line arguments (without the name of the programme)
    :param args: list of strings
    :return: :class:`argparse.Namespace`
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--genome',
                        required=True
                        )
    parser.add_argument('--k7-plasmid',
                        required=True,
                        help="The path to the plasmid containing the cassette. in fasta format.")
    parser.add_argument('--k7-cut',
                        required=True,
                        help="The position (1 based) of the cut in the cassette plasmid."
                             "The cut is perform between k7-cut and k7-cut + 1")
    parser.add_argument('--positions',
                        required=True)
    parser.add_argument('--no-seq',
                        action='store_true',
                        default=False,
                        help="do not generate sequences with cassette")
    parser.add_argument('--out-dir',
                        default='Salin_sequences')
    parser.add_argument('--verbosity', '-v',
                        action='count',
                        default=0,
                        help="Increases the verbosity level. There are 4 levels: Error messages (default), "
                             "Warning (-v), Info (-vv) and Debug.(-vvv)")
    parsed_args = parser.parse_args(args)

    level = logging.INFO - (20 * parsed_args.verbosity)
    level = min(50, max(10, level))
    parsed_args.log_level = level
    return parsed_args


def main(args=None):
    """
    iterate over each position and insert the cassette in right orientation in the genome
    write the generated sequence in fasta format in --out-dir

    :param args:
    """
    def empty_dir(path: str) -> bool:
        with os.scandir(path) as it:
            return not any(it)

    global _log
    if args is None:
        args = sys.argv[1:]

    parsed_args = parse_args(args)
    _log.setLevel(parsed_args.log_level)

    genome = SeqIO.read(parsed_args.genome, 'fasta')
    plasmid = SeqIO.read(parsed_args.k7_plasmid, 'fasta')
    k7_cut = int(parsed_args.k7_cut) - 1
    cassette = get_cassette(plasmid, k7_cut)
    k7_len = len(cassette)
    positions = get_positions_of_insertion(parsed_args.positions)
    out_dir = parsed_args.out_dir

    if os.path.exists(out_dir):
        if not os.path.isdir(out_dir):
            raise IOError(f"{out_dir} exists and is not a regular directory")

    seq_dir = os.path.join(out_dir, 'Sequences')
    junc_dir = os.path.join(out_dir, 'Sites')
    junc_file_path = os.path.join(junc_dir, 'sites.tsv')

    if os.path.exists(seq_dir) :
        if not parsed_args.no_seq and not empty_dir(seq_dir):
            raise IOError(f"{seq_dir} already exists and not empty. Remove/Rename it")
    else:
        os.makedirs(seq_dir)

    if os.path.exists(junc_dir):
        if os.path.exists(junc_file_path):
            raise IOError(f"{junc_file_path} already exists. Remove/Rename it.")
    else:
        os.makedirs(junc_dir)

    cas_inj = CassetteInjector(genome, cassette)

    with (open(junc_file_path, 'w') as junct_file):
        junct_file.write("ID_replicon\tpos\torient\tsite_3\tsite_10\t"
                         "upstream_3\tupstream_10\tdownstream_3\tdownstream_10\n")
        for idx, pos, orientation in zip(positions.index.to_list(),
                                  positions['Position'].to_list(),
                                  positions['orient'].to_list()):

            _log.info(f"insert cassette @ {pos} in {orientation}")
            seq = cas_inj.insert(idx, pos, orientation)
            if not parsed_args.no_seq:
                SeqIO.write(seq, os.path.join(out_dir, f"{seq.id}.fasta"), 'fasta')

            sites_row = [seq.id, str(pos + 1), orientation]  # pos in tsv must be 1-based
            sites_row += [str(s.seq) for s in get_insertion_site(genome, pos)]
            upstream_3, upstream_10, downstream_3, downstream_10 = get_junctions(seq, pos, k7_len, orientation)
            if orientation == 'forward':
                sites_row += [str(s.seq) for s in (upstream_3, upstream_10, downstream_3, downstream_10)]
            elif orientation == 'reverse':
                sites_row += [str(s.reverse_complement().seq) for s in (upstream_3, upstream_10, downstream_3, downstream_10)]
            else:
                raise RuntimeError(f"Unexpected orientation in positions:{orientation}")
            junct_file.write('\t'.join(sites_row) + '\n')


if __name__ == '__main__':

    main()
