#########################################################################
# Salin - scripts to insert attc casette in genomes                     #
# Authors: Bertrand Neron                                               #
# Copyright (c) 2023  Institut Pasteur (Paris).                         #
#                                                                       #
# This file is part of MacSyFinder package.                             #
#                                                                       #
# MacSyFinder is free software: you can redistribute it and/or modify   #
# it under the terms of the GNU General Public License as published by  #
# the Free Software Foundation, either version 3 of the License, or     #
# (at your option) any later version.                                   #
#                                                                       #
# salin is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of        #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          #
# GNU General Public License for more details .                         #
#                                                                       #
# You should have received a copy of the GNU General Public License     #
# along with salin (COPYING).                                           #
# If not, see <https://www.gnu.org/licenses/>.                          #
#########################################################################

import logging
from typing import Tuple

import pandas as pd
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio import SeqIO

_log = logging.getLogger(__file__)


class CassetteInjector:
    """
    This class handle cassette injection in genome at different position and orientation
    """

    def __init__(self, genome: SeqRecord, cassette: SeqRecord) -> None:
        """

        :param genome: The genome in which the cassette will be inserted
        :type genome: `class`:Bio.SeqRecord.SeqRecord`
        :param cassette: The cassette to insert n the genome
        :type cassette: `class`:Bio.SeqRecord.SeqRecord`
        """
        assert isinstance(genome.seq, Seq), f"genome.seq must be a Bio.Seq instance not {type(genome.seq)}"
        assert isinstance(cassette.seq, Seq), f"cassette.seq must be a Bio.Seq instance not {type(cassette.seq)}"
        self.genome = genome
        self.for_cas = cassette
        self.rev_cas = cassette.reverse_complement()


    def _get_id(self, idx: int, sens: str, position: int, site: str) -> str:
        """

        :param int idx: unique identifier of the sequence
        :param str sens: the sens of the insertion cassette ('Forward' | 'Reverse')
        :param int position: position of the insertion (zero based)
        :param str site: the site of insertion GTT | GAT , ...
        :return: an uniq id for the sequence with the following format
                 ident_(rev|for)_<position>
        """
        return f"{idx:05d}_{sens[:3].lower()}_{position + 1}_{site}"


    def insert(self, idx: int, position: int, sens: str) -> SeqRecord:
        """
        insert the cassette in the genome

        - When insertion is made in forward the cassette is inserted before the position
        - When insertion is made in reverse the cassette is reverse complemented and inserted after the position

        :param int idx: unique identifier of the sequence
        :param int position: position of the insertion (zero based)
        :param str sens: the sens of the inserted cassette ('forward' | 'reverse')
        :return: The generated the genome with the cassette inserted at the position
        :rtype: :class:`biopython.SeqRecord.SeqRecord` object
        """
        if sens == 'forward':
            offset = 0
            insert = self.for_cas
        elif sens == 'reverse':
            offset = 1
            insert = self.rev_cas
        else:
            raise RuntimeError("Invalid value for sense parameter ('forward'| 'reverse') got: {sense}")
        site = self.genome.seq[position - 1: position + 2]

        before_insert = self.genome[: position + offset]
        after_insert = self.genome[position + offset:]
        with_insert = before_insert + insert + after_insert
        seq_id = self._get_id(idx, sens, position, site)
        with_insert.id = seq_id
        with_insert.name = seq_id
        # the positions in the seq comment are 1 based
        with_insert.description = f"insertion between {position + offset}/{position + offset + 1} " \
                                  f"in '{sens[:3]}' site: {site}"
        _log.debug(with_insert.description)
        return with_insert


def get_cassette(plasmid: SeqRecord, cut: int) -> SeqRecord:
    """

    :param plasmid: the path to the fasta file containing the plasmid wit the attc site
    :type plasmid: :class:`Seq.SeqRecord` object
    :param int cut: the position of the nucleotide where to cut (cut after) (position 0 based)
    :return: The cassette to insert in genome (in forward)
    :rtype: class:`Seq.SeqRecord` object
    """
    ori_to_cut = plasmid[:cut + 1]
    cut_to_end = plasmid[cut + 1:]
    k7_for = cut_to_end + ori_to_cut
    return k7_for


def get_positions_of_insertion(path: str) -> pd.DataFrame:
    """

    :param str path: the path to file containing positions (tabulated file with columns  'Position' and 'orient'.
    :return: the position of insertion with the orientation of the insertion ('Forward' | 'Reverse')
             The positions are Zero based.
    :rtype: :class:`pandas.DataFrame` object with 2 columns ['Position' and 'orient']
    """
    insertions = pd.read_csv(path, sep='\t')
    insert_pos = insertions.loc[:, ['Position', 'orient']]  # it makes a copy not a slice
    insert_pos.loc[:, 'Position'] -= 1  # so I can now mutate it
    insert_pos.orient = insert_pos.orient.str.lower()
    return insert_pos


def get_insertion_site(genome: SeqRecord, pos: int) -> str:
    """

    :param genome: The genome before the cassette injection
    :param pos: The position of injection (zero based)
    :return: the sites
             [-1 : 1] and [-5:4] included of the genome
    """
    site_3 = genome[pos - 1: pos + 2]
    site_10 = genome[pos - 5: pos + 5]
    return site_3, site_10


def get_junctions(seq: SeqRecord, pos: int, k7_len: int, orient: str) -> Tuple[SeqRecord, SeqRecord, SeqRecord, SeqRecord]:
    """

    :param seq: The genome with the cassette
    :param pos: The position of injection (zero based)
    :param k7_len: The cassette length
    :param orient: the orientation of the insertion ('forward' | 'reverse')
    :return: the junctions between Ecoli and the cassette
             * in 5'
                 * 2 nt in genome 1 in cassette (position is last nt in genome)
                 * 7 nt in genome 3 nt cassette
              *in 3'
                 * 1 nt in cassette and 2 nt in genome
                 * 3 nt in cassette and 7 nt in genome
    :rtype: tuple with 4 Bio.SeqRecord.SeqRecord
    """
    offset = 0
    if orient == 'reverse':
        offset += 1

    upstream_3 = seq[pos - 1:pos + 2]
    upstream_10 = seq[pos - 7 + offset:pos + 3 + offset]
    downstream_3 = seq[pos + k7_len - 1:pos + k7_len + 2]
    downstream_10 = seq[pos + k7_len - 3 + offset: pos + k7_len + 7 + offset]
    return upstream_3, upstream_10, downstream_3, downstream_10


def junctions_to_fasta(data: pd.DataFrame, loc: str, orient: str, path: str) -> Tuple[int, str]:
    """
    extract sequence from data and generate a fasta alignment file ready to use weblogo
    :param data: the junctions between genome and cassette
    :param loc: 'upstream' | 'downstream'
    :param orient: the sens of insertion 'forward' | 'reverse'
    :param path: the path to the file to store the sequence
    :return: the number of sequence in fasta file, the path of the generated file
    """
    all_seq = []
    data = data.loc[:, ['ID_replicon', 'pos', 'orient', 'seq']]
    for idx, row in data.iterrows():
        seq = SeqRecord(Seq(row.seq),
                        id=f"{row['ID_replicon']}_{loc}_{len(row.seq)}_{orient[:3]}",
                        description=f"insertion in {orient} site: {loc}_{len(row.seq)}",
                        )
        seq.name = seq.id
        all_seq.append(seq)
    seq_nb = SeqIO.write(all_seq, path, "fasta")
    return seq_nb, path


