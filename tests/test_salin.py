#########################################################################
# Salin - scripts to insert attc casette in genomes                     #
# Authors: Bertrand Neron                                               #
# Copyright (c) 2023  Institut Pasteur (Paris).                         #
#                                                                       #
# This file is part of MacSyFinder package.                             #
#                                                                       #
# MacSyFinder is free software: you can redistribute it and/or modify   #
# it under the terms of the GNU General Public License as published by  #
# the Free Software Foundation, either version 3 of the License, or     #
# (at your option) any later version.                                   #
#                                                                       #
# salin is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of        #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          #
# GNU General Public License for more details .                         #
#                                                                       #
# You should have received a copy of the GNU General Public License     #
# along with salin (COPYING).                                           #
# If not, see <https://www.gnu.org/licenses/>.                          #
#########################################################################

import pandas as pd
from Bio.SeqRecord import SeqRecord
from Bio.Seq import Seq

from salin import salin
from salin.scripts import salin_gen
from tests import SalinTest


class TestSalinPackage(SalinTest):

    def test_CassetteInjector(self):
        genome = SeqRecord("AATTAA")
        cassette = SeqRecord("GGCCGG")
        with self.assertRaises(AssertionError) as ctx:
            salin.CassetteInjector(genome, cassette)
        self.assertEqual(str(ctx.exception),
                         f"genome.seq must be a Bio.Seq instance not {type(genome.seq)}")

        genome = SeqRecord(Seq("AATTAA"))
        with self.assertRaises(AssertionError) as ctx:
            salin.CassetteInjector(genome, cassette)
        self.assertEqual(str(ctx.exception),
                         f"cassette.seq must be a Bio.Seq instance not {type(cassette.seq)}")

        cassette = SeqRecord(Seq("GGCCGG"))

        ci = salin.CassetteInjector(genome, cassette)
        self.assertEqual(ci.genome.seq, genome.seq)
        self.assertEqual(ci.for_cas.seq, cassette.seq)
        self.assertEqual(ci.rev_cas.seq, cassette.reverse_complement().seq)


    def test_insert(self):
        genome = SeqRecord(Seq("AATTAA"))
        cassette = SeqRecord(Seq("GGCCGG"))
        ci = salin.CassetteInjector(genome, cassette)
        # in forward the cut is before the position
        seq = ci.insert(1, 3, "forward")
        self.assertEqual(seq.seq,
                         Seq('AATGGCCGGTAA'))
        self.assertEqual(seq.description,
                         "insertion between 3/4 in 'for' site: TTA")
        self.assertEqual(seq.id, '00001_for_4_TTA')
        # in reverse the cut is after the position
        seq = ci.insert(3, 2, "reverse")
        self.assertEqual(seq.seq,
                         Seq('AATCCGGCCTAA'))
        self.assertEqual(seq.description,
                         "insertion between 3/4 in 'rev' site: ATT")
        self.assertRegex(seq.id, '00003_rev_3_ATT')


    def test_get_positions_of_insertion(self):
        insertions = salin.get_positions_of_insertion(self.find_data('insertions.freq'))
        expected = pd.read_csv(self.find_data('expected_insertions.csv'), index_col=[0])
        pd.testing.assert_frame_equal(insertions, expected)

    def test_get_cassette(self):
        plasmid = SeqRecord(Seq("AAAACCCCGGGGTTTT"))
        cut = 3  # cut after the AAAA before the CCCC
        k7 = salin.get_cassette(plasmid, cut)
        expected_seq = Seq("CCCCGGGGTTTTAAAA")
        self.assertEqual(k7.seq, expected_seq)

    def test_get_insertion_site(self):
        genome = SeqRecord(Seq("cCAAATTAAACc"))
        sites = salin.get_insertion_site(genome, 5)
        self.assertEqual([s.seq for s in sites], ['ATT', 'cCAAATTAAA'])

    def test_get_junctions(self):
        k7 = 'G' * 10
        seq = SeqRecord(Seq('CC' + 'AT' * 5 + k7 + 'AT' * 5 + 'CC'))
        # insertion is between nt 11 and 12
        # so pos = 12 (zero-based)
        sites = salin.get_junctions(seq, 12, len(k7))
        sites = [str(s.seq) for s in sites]
        self.assertEqual(sites, ['ATG', 'TATATATGGG', 'GAT', 'GGGATATATA'])


    def test_parse_args(self):
        g_path = "path/to/genome"
        c_path = "path/to/cassette"
        k7_cut = 388
        pos = "path/to/positions/file"
        out_dir = "outdir"

        parsed_args = salin_gen.parse_args(f"--genome {g_path} --k7-plasmid {c_path} --k7-cut {k7_cut} "
                                           f"--positions {pos}  --out-dir {out_dir}".split())
        self.assertEqual(parsed_args.genome, g_path)
        self.assertEqual(parsed_args.k7_plasmid, c_path)
        self.assertEqual(parsed_args.k7_cut, str(k7_cut))
        self.assertEqual(parsed_args.positions, pos)
        self.assertEqual(parsed_args.out_dir, out_dir)
        self.assertEqual(parsed_args.log_level, 20)

        parsed_args = salin_gen.parse_args(f"--genome {g_path} --k7-plasmid {c_path} --k7-cut {k7_cut} "
                                           f"--positions {pos} -v".split())
        self.assertEqual(parsed_args.out_dir, 'Salin_sequences')

        self.assertEqual(parsed_args.log_level, 10)
        self.assertEqual(parsed_args.no_seq, False)

        parsed_args = salin_gen.parse_args(f"--genome {g_path } --k7-plasmid {c_path} --k7-cut {k7_cut} "
                                           f"--positions {pos} --no-seq".split())

        self.assertEqual(parsed_args.no_seq, True)

    def test_main(self):
        pass

