# Salin

## Installation

    python3.10 -m venv salin_env
    source salin_env/bin/activate
    mkdir src
    cd src
    git clone git@gitlab.pasteur.fr:hub/salin.git
    cd salin
    python -m pip install -e .
    

## Sequences with attCaadA7 cassette generation

    cd salin_env
    source bin/activate
    cd src/salin/
    
    salin_gen --genome data/Ecoli-K12-MG1655_ORI_CENTERED.fasta --k7-plasmid data/cassette_attCaadA7.fasta --k7-cut 388 \
    --positions data/Pool-B2699_S1_L001_R1_001.fastq_q20_nodup_annot.freq --no-seq -vv


## Genomes analyses with Integron_finder

### nextflow installation

We need java to be installed. 
We used java/13.0.2

    cd salin_env/bin
    curl -s https://get.nextflow.io | bash
    
add salin_env/bin in the PATH

### Command nextflow

    cd salin_env
    source bin/activate
    cd src/salin

    nextflow run salin.nf run -profile standard cluster_apptainer -with-report salin_report.html -with-trace -with-timeline salin_timeline \
    --local-max --keep-tmp --calin-threshold=1 --promoter-attI --func-annot --cpu 4 --replicons 'Salin_sequences/*.fasta' 

### Results

The results are analysed through a jupyter-lab and are accessible in [Results dir](Results/) .
The results of the Integron_Finder on all sequences are available in:

 - Results/salin_all_seq.summary
 - Results/salin_all_seq.integrons

A not interactive overview can be open directly in the browser [salin_results](Results/salin_results.ipynb)