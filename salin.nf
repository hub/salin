#!/usr/bin/env nextflow

/************************************************************************
# Salin - scripts to insert attc casette in genomes                     #
# Authors: Bertrand Neron                                               #
# Copyright (c) 2023  Institut Pasteur (Paris).                         #
#                                                                       #
# This file is part of MacSyFinder package.                             #
#                                                                       #
# MacSyFinder is free software: you can redistribute it and/or modify   #
# it under the terms of the GNU General Public License as published by  #
# the Free Software Foundation, either version 3 of the License, or     #
# (at your option) any later version.                                   #
#                                                                       #
# salin is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of        #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the          #
# GNU General Public License for more details .                         #
#                                                                       #
# You should have received a copy of the GNU General Public License     #
# along with salin (COPYING).                                           #
# If not, see <https://www.gnu.org/licenses/>.                          #
*************************************************************************/

nextflow.enable.dsl = 2

/*************************
 * Default options
 *************************/

params.gbk = false
params.pdf = false
params['local-max']= false
params['func-annot'] = false
params['distance-threshold'] = false
params['union-integrases'] = false
params['path-func-annot'] = false
params['attc-model'] = false
params['evalue-attc'] = false
params['keep-palindrome'] = false
params['no-proteins'] = false
params['promoter-attI'] = false
params['max-attc-size'] = false
params['min-attc-size'] = false
params['keep-tmp'] = false
params['calin-threshold'] = false
params['cmsearch'] = false
params['hmmsearch'] = false
params['prodigal'] = false
params.debug = false

/****************************************
*  parameter not really used in the wf  *
*  used to improve UI                   *
*****************************************/
params.help = false
params.profile = false

/****************************************
*             real parameters           *
*****************************************/
gbk = params.gbk ? '--gbk' : ''
pdf = params.pdf ? '--pdf' : ''
local_max = params['local-max'] ? '--local-max' : ''
func_annot = params['func-annot'] ? '--func-annot' : ''
path_func_annot = params['path-func-annot'] ? "--path-func-annot ${params['path-func-annot']}" : ''
dist_thr = params['distance-threshold'] ? "--distance-thresh ${params['distance-threshold']}" : ''
union_integrases = params['union-integrases'] ? '--union-integrase' : ''
attc_model = params['attc-model'] ? "--attc-model ${params['attc-model']}" : ''
evalue_attc = params['evalue-attc'] ? "--evalue-attc ${params['evalue-attc']}" : ''
keep_palindrome = params['keep-palindrome'] ? '--keep-palindrome' : ''
no_proteins = params['no-proteins'] ? '--no-proteins' : ''
promoter = params['promoter-attI'] ? '--promoter-attI' : ''
max_attc_size = params['max-attc-size'] ? "--max-attc-size ${params['max-attc-size']}" : ''
min_attc_size = params['min-attc-size'] ? "--min-attc-size ${params['min-attc-size']}" : ''
keep_tmp = params['keep-tmp'] ? '--keep-tmp' : ''
calin_threshold = params['calin-threshold'] ? "--calin-threshold ${params['calin-threshold']}" : ''
cmsearch = params['cmsearch'] ? "--cmsearch ${params['cmsearch']}" : ''
hmmsearch = params['hmmsearch'] ? "--hmmsearch ${params['hmmsearch']}" : ''
prodigal = params['prodigal'] ? "--prodigal ${params['prodigal']}" : ''
debug = params.debug ? '-vv' : ''


if (params.help){
   msg = '''
parallel_integron_finder available options:

 --gbk
 --pdf
 --local-max
 --func-annot
 --distance-threshold
 --union-integrases
 --path-func-annot
 --attc-model
 --evalue-attc
 --keep-palindrome
 --no-proteins
 --promoter-attI
 --max-attc-size
 --min-attc-size
 --keep-tmp
 --calin-threshold
 --replicons
 --cmsearch
 --prodigal
 --hmmsearch

Please refer to the integron_finder documentation for the meaning of each options.
'''
    println(msg)
    System.exit(0)
}
if (params.profile){
    throw new Exception("The integron_finder option '--profile' does not exists. May be you want to use the nextflow option '-profile'.")
}
if (! params.replicons){
    throw new Exception("The option '--replicons' is mandatory.")
}

if (params.replicons.contains(',')){
        paths = params.replicons.tokenize(',')
    } else {
        paths = params.replicons
    }


/****************************************
 *           The processes              *
 ****************************************/


process integron_finder{

    input:
        path one_replicon
        val gbk
        val pdf
        val local_max
        val func_annot
        val path_func_annot
        val dist_thr
        val union_integrases
        val attc_model
        val evalue_attc
        val keep_palindrome
        val no_proteins
        val promoter
        val max_attc_size
        val min_attc_size
        val keep_tmp
        val calin_threshold
        val debug
        val cmsearch
        val hmmsearch
        val prodigal
    output:
        path "Results_Integron_Finder_${one_replicon.baseName}"

    script:
        
        """
        integron_finder ${local_max} ${func_annot} ${path_func_annot} ${dist_thr} ${union_integrases} ${attc_model} ${evalue_attc} ${keep_palindrome} ${no_proteins} ${promoter} ${max_attc_size} ${min_attc_size} ${calin_threshold} --circ ${gbk} ${pdf} ${keep_tmp} --cpu ${task.cpus} ${cmsearch} ${hmmsearch} ${prodigal} --mute ${debug} ${one_replicon}
        """
}



process merge_results{

    publishDir ".", mode: 'copy'

    input:
        path all_chunk_results

    output:
        path  "Results_Integron_Finder_Salin"
        
    script:
        """
        integron_merge "Results_Integron_Finder_Salin" salin_all_seq ${all_chunk_results}
        """
}


/****************************************
 *           The workflow               *
 ****************************************/

workflow {

    replicons_files = Channel.fromPath(paths)

    results_per_replicon = integron_finder(replicons_files.flatten(), gbk, pdf,local_max, func_annot, path_func_annot,
    dist_thr, union_integrases, attc_model, evalue_attc, keep_palindrome, no_proteins, promoter,
    max_attc_size, min_attc_size, keep_tmp, calin_threshold, debug, cmsearch, hmmsearch, prodigal)

    grouped_results = results_per_replicon.toList()

    merged_results = merge_results(grouped_results)
}


workflow.onComplete {
    if ( workflow.success )
        println("\nDone!")
}

workflow.onError {
    println "Oops .. something went wrong"
    println "Pipeline execution stopped with the following message: ${workflow.errorMessage}"
}
